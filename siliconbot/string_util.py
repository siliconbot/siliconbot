#!/usr/bin/env python3

import nltk
from typing import List, Iterable, Callable, Generator
from itertools import takewhile


# Load NLTK data, download if not already present
try:
    sentence_detector = nltk.data.load('tokenizers/punkt/english.pickle', verbose=True)
except LookupError:
    nltk.download('punkt')
    sentence_detector = nltk.data.load('tokenizers/punkt/english.pickle', verbose=True)


def truncated_by_sentence(text):
    """
    Truncate a string while trying to keep sentences complete

    If the first sentence is longer than 150 chars, return it, truncated to 200 chars.
    Else return as many as possible of the sentences, without exceeding 150 chars.
    """

    sentences = sentence_detector.tokenize(text)

    return concat_w_maxlen(sentences, 200, 150)


def concat_w_maxlen(strings: List[str], single_string_limit: int, multiple_string_limit = None) -> str:
    """
    Build a text of strings while staying withing length limits

    If the first string is longer than multiple_string_limit chars, return it, truncated to
    single_string_limit chars. Else return as many as possible of the strings, joined with
    spaces, without exceeding multiple_string_limit chars.

    >>> concat_w_maxlen( [], 20, 15 )
    ''
    >>> concat_w_maxlen( [""], 20, 15 )
    ''
    >>> concat_w_maxlen( ["Hello, my length is 23.", "Foo bar."], 20, 15 )
    'Hello, my length is…'
    >>> concat_w_maxlen( ["Hello, my length ", "Foo bar."], 20, 15 )
    'Hello, my length '
    >>> concat_w_maxlen( ["Phrase one", "two", "three"], 20, 15 )
    'Phrase one two'
    >>> concat_w_maxlen( ["Phrase one", "two", "three", "four", "a"], 20, 15 )
    'Phrase one two'
    >>> concat_w_maxlen( ["Sentence len 15", "len4", "foo", "bar", "a"], 20, 15 )
    'Sentence len 15'
    >>> concat_w_maxlen( ["one", "two", "three", "four", "five"], 20, 15 )
    'one two three'
    >>> concat_w_maxlen( ["Hello, first phrase.", "Second phrase."], 15, 20 )
    'Hello, first p…'
    >>> concat_w_maxlen( ["Hello, first.", "2nd."], 15, 20 )
    'Hello, first. 2nd.'
    >>> concat_w_maxlen( ["one", "two", "three", "four", "five"], 15, 20 )
    'one two three four'
    """

    # Argument default
    if multiple_string_limit is None:
        multiple_string_limit = single_string_limit

    return "" if not strings else (
        truncate_with_ellipsis(strings[0], single_string_limit)
        if len(strings[0]) >= multiple_string_limit
        else concat_first_n(strings,
                            n = last_fulfilling(
                                lambda x: x[1] <= multiple_string_limit,
                                enumerate(lengths_of_concats(strings), 1)
                            )[0]
        )
    )


def last_fulfilling(predicate, iterable):
    return last(takewhile(predicate, iterable))


def lengths_of_concats(strings, joiner_length = 1):
    """
    An iterator where the index number maps to the length of the concatenation of the corresponding
    and all previous items in strings

    >>> tuple(lengths_of_concats( [] ))
    ()
    >>> tuple(lengths_of_concats( ["foobar"] ))
    (6,)
    >>> tuple(lengths_of_concats( ["one", "two", "three"] ))
    (3, 7, 13)
    """

    # With +joiner_length, we count a joiner before each element, also the first one. We substract
    # that again later
    string_lengths = ( len(s) + joiner_length for s in strings )

    # Compensate for the joiner not being added before the first element
    return sums(string_lengths, -joiner_length)


def concat_first_n(strings: List[str], n: int, joiner = " ") -> str:
    return joiner.join(strings[:n])


def sums(iterable, initial = None):
    """
    >>> tuple( sums([2, 4, 0, 10]) )
    (2, 6, 6, 16)
    >>> tuple( sums([]) )
    ()
    """
    return fold_map(lambda x,y: x+y, iterable, initial)


def truncate_with_ellipsis(string: str, max_length: int):
    return string[:max_length-1] + "…" if len(string) > max_length else string


# Non-functional zone
## We're creating some primitives here to use in the functional zone. It's not feasible to write
## these functionally in Python.

def fold_map(function: Callable, iterable, initial = None) -> Generator:
    """
    Similar to reduce (aka fold), but returns a list of successive reduced values from the left

    Haskell equivalent: scanl

    >>> tuple( fold_map(lambda x,y: x+y, [2, 4, 0, 10]) )
    (2, 6, 6, 16)
    >>> tuple( fold_map(lambda x,y: x+y, [2, 4, 0, 10], 10) )
    (12, 16, 16, 26)
    >>> tuple( fold_map(lambda x,y: x+y, []) )
    ()
    >>> tuple( fold_map(lambda x,y: x+y, [], 10) )
    ()
    """
    it = iter(iterable)

    if initial is not None:
        accumulator = initial
    else:
        try:
            accumulator = next(it)
            yield accumulator
        except StopIteration:
            return

    for element in it:
        accumulator = function(accumulator, element)
        yield accumulator


def last(iterable):
    item = None
    for item in iterable:
        pass
    return item


if __name__ == "__main__":
    import doctest
    doctest.testmod()
