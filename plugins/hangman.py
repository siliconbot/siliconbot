#!/usr/bin/env python3

from random import choice
from siliconbot.command import Command


TOLERANCE = 7

with open('./hangman_words.txt', 'r') as _word_list_file:
    WORD_LIST = tuple(
        map(lambda x: x.rstrip('\n').lower(), _word_list_file.readlines())
    )

chat_games = {}


def chat_get_game(_match, metadata, _bot):
    adapter_id = metadata["_id"]
    channel = metadata["from_group"]

    if adapter_id not in chat_games:
        chat_games[adapter_id] = {}
        return None

    if channel not in chat_games[adapter_id]:
        return None

    return chat_games[adapter_id][channel]


def chat_new_game(_match, metadata, _bot):
    adapter_id = metadata["_id"]
    channel = metadata["from_group"]

    if adapter_id not in chat_games:
        chat_games[adapter_id] = {}

    word = choice(WORD_LIST)
    game = {
        "pattern": '_' * len(word),
        "previous_letters": set(),
        "misses_left": TOLERANCE,
        "word": word
    }
    chat_games[adapter_id][channel] = game
    return game


def chat_end(_match, metadata, _bot):
    adapter_id = metadata["_id"]
    channel = metadata["from_group"]

    if channel in chat_games.get(adapter_id, {}):
        del chat_games[adapter_id][channel]


def chat_start(match, metadata, bot):
    game = chat_get_game(match, metadata, bot)

    if game:
        bot.send("Send a message consisting of one letter to make a guess.",
                 metadata['from_group'], metadata['_id'])
    else:
        game = chat_new_game(match, metadata, bot)

        bot.reply(
            "Started a new hangman game in English! The word is {word_len} letters long. "
            "Guess a letter…".format(word_len=len(game["word"])),
            metadata['message_id'], metadata['from_group'], metadata['_id']
        )

    chat_status(match, metadata, bot)


def chat_guess(match, metadata, bot):
    game = chat_get_game(match, metadata, bot)

    if not game:
        if match.group('cmd'):
            bot.reply("There's no hangman game going on here! Start one first by doing !hangman",
                      metadata['message_id'], metadata['from_group'], metadata['_id'])
        return

    guessed_letter = match.group("letter").lower()
    if guessed_letter == "_":
        bot.reply("No words contain underscores. Lol, that'd be confusing.",
                  metadata['message_id'], metadata['from_group'], metadata['_id'])
        chat_status(match, metadata, bot)
        return

    if guessed_letter in game["previous_letters"]:
        bot.reply(("You already guessed {letter}"
                   if guessed_letter in game["pattern"]
                   else "You already tried {letter}"
                   ).format(letter=guessed_letter.upper()),
                  metadata['message_id'], metadata['from_group'], metadata['_id'])
        return

    game["pattern"] = "".join(
        c_word if c_pattern != "_" or c_word == guessed_letter else "_"
        for c_pattern, c_word in zip(game["pattern"], game["word"])
    )
    game["previous_letters"].add(guessed_letter)
    if guessed_letter not in game["pattern"]:
        game["misses_left"] -= 1

    if '_' not in game["pattern"]:
        # Guessed the word!
        chat_status(match, metadata, bot)
        chat_end(match, metadata, bot)
        bot.reply("Congratulations, you guessed the word!",
                  metadata['message_id'], metadata['from_group'], metadata['_id'])
        return

    if not game["misses_left"]:
        chat_status(match, metadata, bot)
        chat_end(match, metadata, bot)
        bot.reply("Game over! The word was {}.".format(game["word"]),
                  metadata['message_id'], metadata['from_group'], metadata['_id'])
        return

    chat_status(match, metadata, bot)


def chat_status(match, metadata, bot):
    game = chat_get_game(match, metadata, bot)

    bot.send('{pattern}   {left}/{total}'.format(pattern=" ".join(game["pattern"].upper()),
                                                 left=game["misses_left"],
                                                 total=TOLERANCE
                                                 ),
             metadata['from_group'], metadata['_id']
             )


def register_with(silicon):
    silicon.add_commands(
        Command(r"{ident}hangman(?: .*)?",
                "hangman",
                "Play a game of hangman: guess a word one letter at a time",
                chat_start
                ),
        Command(r"(?P<cmd>{ident}guess )?(?P<letter>[A-Za-z_])",
                "", "",
                chat_guess,
                display_condition=lambda message, metadata, bot: False,
                ),
    )
